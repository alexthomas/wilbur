import {init} from "wilbur/utility/dataGenerator";
import {astraClient} from "wilbur/utility/AstraClient";
import {Equipment} from "wilbur/types/Equipment";

const dataGeneratorPromise = init();

export async function getEquipment(initial:string,number:string,company?:string){
    try{

    const lookupResponse = await astraClient.get<Equipment>(`equipment/${initial}-${number}?raw=true`);
    console.log(lookupResponse.status)
    if(lookupResponse.data && lookupResponse.status===200){
        if(company && lookupResponse.data.company !== company){
            return null;
        }
        return lookupResponse.data;
    }
    } catch (e){
        console.info(e)
    }
    if(Math.random()<.2){
        return null;
    }
    const dataGenerator = await dataGeneratorPromise;
    const equipment = dataGenerator.generateEquipment();
    equipment.initial = initial;
    equipment.number = number;
    if(company){
        equipment.company = company;
    }
    await astraClient.put(`equipment/${initial}-${number}?ttl=${3600*24*7}`,JSON.stringify(equipment),{headers:{'Content-Type':'application/json'}});
    return equipment;

}