import {Catalog} from "wilbur/types/Catalog";
import {astraClient} from "wilbur/utility/AstraClient";

export async function getCatalogs(): Promise<Catalog> {
    const response = await astraClient.get<Catalog>(`/catalog?page-size=10&raw=true`)
    return response.data;
}

export async function getCatalog(id:string): Promise<any>{
    const response = await astraClient.get(`/catalog/${id}?raw=true`)
    return response.data;
}
