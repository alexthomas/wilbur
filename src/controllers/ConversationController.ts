import {astraClient} from "wilbur/utility/AstraClient";
import {
    ChatCompletionFunctions,
    ChatCompletionRequestMessage,
    ChatCompletionRequestMessageRoleEnum,
    Configuration,
    OpenAIApi
} from "openai";

const configuration = new Configuration({
    apiKey: process.env.OPENAI_TOKEN,
});
const openai = new OpenAIApi(configuration);

const INITIAL_MESSAGES: ChatCompletionRequestMessage[] = [
    {
        role: "system",
        content: "You are a virtual assistant named Mr.Ed for tracking rail shipments. When users users provide you with the correct criteria you should search for the shipment and summarize the results. All timestamps should be in EST"
    },
    {
        role: "assistant",
        content: "Hello, I'm Mr.Ed. I'm here to help you track your shipments. Please provide me with the criteria you would like to search by, such as the shipment ID, origin, or destination."
    },
];

const FUNCTIONS: ChatCompletionFunctions[] = [
    {
        name: "search",
        description: "Search for a shipment",
        parameters:
            {
                type: "object",
                properties: {
                    type: {type: "string", "enum": ["equipment", "waybill", "origin", "destination"]},
                    equipmentId: {type: "string", pattern: "[A-Z]{2,4}[0-9]{6}"},
                    waybillSerialNumber: {type: "string", pattern: "[0-9]{10}"},
                    origin: {type: "string"},
                    destination: {type: "string"}
                },
                required: ["type"]
            }


    }
]

export async function startConversation(): Promise<{}> {
    const data = {startedDate: new Date().toISOString(), messages: INITIAL_MESSAGES, usages: []};
    const response = await astraClient.post<{
        documentId: string
    }>(`/conversations`, JSON.stringify(data), {headers: {"Content-Type": "application/json"}})
    return {id: response.data.documentId, ...data};
}

export async function getConversation(id:string):Promise<any>{
    return (await astraClient.get(`/conversations/${id}?raw=true`)).data;
}

export async function nextConversationStep(conversationId: string, messages: {
    role: ChatCompletionRequestMessageRoleEnum,
    content: string
}[]): Promise<any[]> {
    if (!messages)
        throw new Error("Messages are required");
    await astraClient.put(`/conversations/${conversationId}/messages?ttl-auto`, JSON.stringify(messages), {headers: {"Content-Type": "application/json"}});
    const completionResponse = await openai.createChatCompletion({
        model: "gpt-3.5-turbo-0613",
        messages,
        functions: FUNCTIONS,
    });
    const newMessage = completionResponse.data.choices[0].message;
    await astraClient.post(`/conversations/${conversationId}/messages/function`, JSON.stringify({
        operation: "$push",
        value: newMessage
    }), {headers: {"Content-Type": "application/json"}});
    await astraClient.post(`/conversations/${conversationId}/usages/function`, JSON.stringify({
        operation: "$push",
        value: completionResponse.data.usage
    }), {headers: {"Content-Type": "application/json"}});
    return (await astraClient.get(`/conversations/${conversationId}?raw=true`)).data;
}