export type Equipment = {
    initial: string,
    number: string,
    waybillSerialNumber: string,
    shipmentStatus: string,
    originCity: string,
    originState: string,
    destinationCity: string,
    destinationState: string,
    eta: string,
    lastEventTime: string,
    lastEvent: string,
    lastEventDescription: string,
    lastEventCity: string,
    lastEventState: string,
    company: string
}