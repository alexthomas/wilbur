export type Catalog = {
    cities: string[],
    states: string[],
    companies: string[],
    initials: string[],
    events: {[key: string]: string}
}
