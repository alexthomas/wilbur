import {Router} from 'express';
import {getConversation, nextConversationStep, startConversation} from "wilbur/controllers/ConversationController";

const router = Router();

router.post('/', async (req, res) => {
    const id = await startConversation();
    res.json(id);
});

router.get("/:id", async (req, res) => {
    const conversation = await getConversation(req.params.id);
    res.json(conversation);
});

router.post('/:id/next', async (req, res) => {
    const updated = await nextConversationStep(req.params.id, req.body);
    res.json(updated);
});

export default router;
