import {Router} from 'express';
import {init} from "wilbur/utility/dataGenerator";
import {getEquipment} from 'wilbur/controllers/EquipmentController'

const router = Router();

router.get('/random', async (req, res) => {
    const dataGenerator = await init();
    res.json(dataGenerator.generateEquipment());
});

router.get('/search/:equipmentId', async (req, res) => {
    const [initial,number] = req.params.equipmentId.split('-');
    const equipment = await getEquipment(initial,number,req.query.company as string);
    if(equipment){
        res.json(equipment);
    } else {
        res.status(404).send();
    }
})

export default router;
