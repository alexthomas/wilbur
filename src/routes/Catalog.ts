import {Router} from "express";
import {getCatalog, getCatalogs} from "wilbur/controllers/CatalogController";

const router = Router();

router.get('/', async (req, res) => {
    const catalog = await getCatalogs();
    res.json(catalog);
});

router.get("/:id", async (req, res) => {
    const catalog = await getCatalog(req.params.id);
    res.json(catalog);
});

export default router;
