import {Axios} from "axios";

export const WILBUR_COLLECTIONS_BASE_URL="https://66064766-2312-4e54-9480-e0de5d05b105-us-east-2.apps.astra.datastax.com/api/rest/v2/namespaces/wilbur/collections";
export const astraClient = new Axios({
    headers: {
        'X-Cassandra-Token': process.env.ASTRA_TOKEN,
        "accept": "application/json"
    },
    baseURL: WILBUR_COLLECTIONS_BASE_URL,
    responseType: 'json',
    transformResponse: res=>JSON.parse(res)

})
