import {Catalog} from "wilbur/types/Catalog";
import {Equipment} from "wilbur/types/Equipment";
import {astraClient} from "wilbur/utility/AstraClient";
import {getCatalogs} from "wilbur/controllers/CatalogController";

export class DataGenerator {
    constructor(private catalog: Catalog) {
    }

    generateEquipment(): Equipment {
        const lastEvent = this.selectRandom(Object.keys(this.catalog.events));
        return {
            initial: this.selectRandom(this.catalog.initials),
            number: this.getRandomNumber(6),
            waybillSerialNumber: this.getRandomNumber(10),
            shipmentStatus: this.selectRandom(['ONTIME', 'DELAYED', 'EARLY', 'UNKNOWN',"DELIVERED"]),
            originCity: this.selectRandom(this.catalog.cities),
            originState: this.selectRandom(this.catalog.states),
            destinationCity: this.selectRandom(this.catalog.cities),
            destinationState: this.selectRandom(this.catalog.states),
            eta: this.getRandomDate(10, 3),
            lastEventTime: this.getRandomDate(2, 2.1),
            lastEvent,
            lastEventDescription: this.catalog.events[lastEvent],
            lastEventCity: this.selectRandom(this.catalog.cities),
            lastEventState: this.selectRandom(this.catalog.states),
            company: this.selectRandom(this.catalog.companies)
        }
    }

    private getRandomNumber(length: number): string {
        let number = '';
        for (let i = 0; i < length; i++) {
            number += Math.floor(Math.random() * 10);
        }
        return number;
    }

    private selectRandom(array: string[]): string {
        return array[Math.floor(Math.random() * array.length)];
    }

    private getRandomDate(rangeDays:number,offsetDays:number):string{
        const now = new Date().getTime();
        const dayMillis = 1000 * 60 * 60 * 24;
        const delta = Math.floor(Math.random() * rangeDays*dayMillis) - offsetDays*dayMillis;
        return new Date(now + delta).toISOString();

    }
}





export async function init(): Promise<DataGenerator> {
    const catalog = await getCatalogs();
    return new DataGenerator(catalog);
}
