import express from 'express'
import bodyParser from "body-parser";
import cors from "cors";

const app = express();
const port = 3000;
app.use(bodyParser.json());
app.use(cors())
app.use('/equipment', require('./routes/Equipment').default);
app.use("/conversation", require("./routes/Conversation").default);
app.use("/catalog", require("./routes/Catalog").default);

app.listen(port, () => {
    console.log(`express server listening at http://localhost:${port}`)
});

