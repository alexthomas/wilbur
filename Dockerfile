FROM node:20
COPY /build /app
COPY package-lock.json /app
COPY package.json /app
WORKDIR /app
RUN npm install --omit=dev
EXPOSE 3000
CMD ["node", "index.js"]